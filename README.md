# A11y

This project uses [Gitlab accessibility testing tool](https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html) to check a11y compliance of websites.

The result is published on Gitlab pages with a custom frontend created with [AlpineJS](https://github.com/alpinejs/alpine) and [TailwindCSS](https://tailwindcss.com/).

## How to use

You need to host a copy of this project (clone or fork it) on a Gitlab instance and modify the `a11y_urls` variable in `.gitlab-ci.yml` to check your own URLs.

## License

AGPLv3. See [LICENSE](LICENSE) file.
