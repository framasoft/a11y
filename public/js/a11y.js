function init() {
    return {
        current: null,
        currentUrl: null,
        hosts: {},
        results: {},
        total: 0,
        passes: 0,
        errors: 0,
        loadReports() {
            let myThis = this;
            fetch('js/gl-accessibility.json')
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response.json();
            })
            .then((res) => {
                let hosts = {};
                for (let i in res.results) {
                    let host = new URL(i).host;
                    if (typeof(hosts[host]) === 'undefined') {
                        hosts[host] = { errors: res.results[i].length, urls: [i] };
                    } else {
                        hosts[host].urls.push(i);
                        hosts[host].errors += res.results[i].length;
                    }
                }
                myThis.hosts   = hosts;
                myThis.results = res.results,
                myThis.total   = res.total,
                myThis.passes  = res.passes,
                myThis.errors  = res.errors
            });
        }
    }
}
